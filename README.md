Die Host-IP/API Server IP als Umgebungsvariable deklarieren:

    export HOST_IP_ADDR="http://<IP>:8000"

Docker-Compose starten

    docker-compose up
