#!/bin/bash

rm /ara-web/public/config.json

touch /ara-web/public/config.json

echo $(jq -n --arg a "$HOST_IP_ADDR" '{"apiURL":$a}') >> /ara-web/public/config.json

cd /ara-web

npm start
