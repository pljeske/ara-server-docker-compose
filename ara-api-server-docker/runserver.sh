#!/bin/bash

export CONTAINER_IP=$(hostname -I | awk '{print $1}')

export ARA_ALLOWED_HOSTS="['$DOCKER_HOST', '0.0.0.0', 'localhost', 'ara-api', '$CONTAINER_IP']"
export ARA_CORS_ORIGIN_ALLOW_ALL=true
export ARA_CORS_ORIGIN_WHITELIST="['http://ara-web:3000','http://localhost:3000', 'http://0.0.0.0:3000']"
export ARA_SETTINGS="/root/.ara/server/settings.yaml"

echo $ARA_ALLOWED_HOSTS
echo $ARA_SETTINGS

ara-manage migrate
ara-manage runserver 0.0.0.0:8000
